## IBM CIC Coding Challenge - REST API

1. Prerequisites
    * Apache Tomcat
    * Maven

2. Used Libraries and Frameworks
    * Jersey  RESTful Web Services Framework (https://jersey.github.io/)
    * JSON in Java (https://github.com/stleary/JSON-java)
    * Apache Commons IO (https://commons.apache.org/proper/commons-io/)
    
3. Running the project 
    * Import the project in IntelliJ IDEA (preferred) or Eclipse
    * Download the dependencies with Maven
    * Start Apache Tomcat

4. Using the API
    * After starting Apache Tomcat point your browser of choice to
     ``http://localhost:8080/rest``
     * You can filter your request by adding the filter parameter as follows:
     ``http://localhost:8080/rest?filter=<title_or_location>``
     * Port ``8080`` is the default one, you can set anything you'd like here
     * You can also access it online (https://ibm-cic.herokuapp.com/rest) 