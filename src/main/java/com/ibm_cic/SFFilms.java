package com.ibm_cic;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

@Path("")
public class SFFilms {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getFilms(@QueryParam("filter") String filter) {
        // create a new JSONArray for the result
        JSONArray result = new JSONArray();

        try {
            // get the JSON as a string
            String jsonString =  IOUtils.toString(new URL("https://data.sfgov.org/resource/wwmu-gmzc.json"), Charset.forName("UTF-8"));

            // transform the string to a JSON array
            JSONArray jsonArray = new JSONArray(jsonString);

            // loop through the array and apply the filter
            for (int i = 0; i < jsonArray.length(); i++)
            {
                // create a temporary JSON object with the value at index i
                JSONObject tmp = jsonArray.getJSONObject(i);

                // create a temporary JSON object for data storage
                JSONObject item = new JSONObject();

                // check if there is a filter
                if(filter == null) { // if we don't have a filter
                    // we always have a title, so we can spare an if here
                    item.put("title", tmp.get("title"));  // add a title to the temporary object
                    if(tmp.has("locations")) // check if there is a location
                        item.put("locations", tmp.get("locations")); // add a location to the temporary object

                    result.put(item); // add the temporary object to the result array
                } else { // if we have a filter
                        if(tmp.get("title").equals(filter)) { // check if the title matches the filter
                            item.put("title", tmp.get("title"));
                            if(tmp.has("locations")) item.put("locations", tmp.get("locations"));
                            result.put(item); // add the temporary object to the result array
                        } else if(tmp.has("locations"))  // check if there is a location
                            if(tmp.get("locations").equals(filter)) { // check if the location matches the filter
                                item.put("title", tmp.get("title"));
                                item.put("locations", tmp.get("locations"));
                                result.put(item); // add the temporary object to the result array
                            }
                }
            }

        } catch(MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return result.toString(); // return the result as a string
    }
}
